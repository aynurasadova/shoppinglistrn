import drawerImg from "../assets/icons/drawerImg.png";
import backArrow from "../assets/icons/backArrow.png"
import newNoteIcon from "../assets/icons/newNote.png";
import saveImg from "../assets/icons/saveImg.png";
import deleteImg from "../assets/icons/deleteImg.png";
import defaultProfileImg from "../assets/icons/defaultProfile.png";

const images = {
    drawerImg,
    backArrow,
    newNoteIcon,
    saveImg,
    deleteImg,
    defaultProfileImg,
}

export default images;