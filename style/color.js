const COLORS = {
    red: "#FF7676",
    lightYellow: "#FFE194",
    yellow: "#FFD976",
    black: "#303234",
    lightGrey: "#EEEEEE",
}

export default COLORS;