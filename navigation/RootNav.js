import React from 'react';
import { NavigationContainer } from "@react-navigation/native";
import { createDrawerNavigator } from "@react-navigation/drawer";

import { CreateScreen, SettingsScreen } from '../screens';
import { CustomDrawer } from '../commons';
import { HomeStack } from './HomeStack';

const {Navigator, Screen} = createDrawerNavigator();

export const RootNav = () => {
    return (
        <NavigationContainer>
            <Navigator 
            drawerStyle = {{width: 287}}
            drawerContent = {(props) => <CustomDrawer {...props}/>}
            initialRouteName = "HomeStack">
                <Screen name = "Create" component = {CreateScreen}/>
                <Screen name = "HomeStack" component = {HomeStack}/>
                <Screen name = "Settings" component = {SettingsScreen}/>
            </Navigator>
        </NavigationContainer>
    )
}