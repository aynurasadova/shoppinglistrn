import React from 'react';
import {createStackNavigator} from "@react-navigation/stack";

import { SingleListDetail, RegularScreen, } from '../screens';
import OneTimeScreen from '../screens/OneTimeScreen';

const { Navigator, Screen } = createStackNavigator();

export const HomeStack = () => {
    return(
        <Navigator headerMode = "none" initialRouteName = "OneTime">
            <Screen name = "OneTime" component = {OneTimeScreen}/>
            <Screen name = "Regular" component = {RegularScreen}/>
            <Screen name = "SingleListDetail" component = {SingleListDetail}/>
        </Navigator>
    )
}