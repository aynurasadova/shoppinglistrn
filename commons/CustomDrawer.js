import React, { useState } from 'react';
import { View, Image, TouchableOpacity, StyleSheet, ScrollView } from 'react-native';

import { DefText } from './DefText';
import COLORS from '../style/color';
import { connect } from 'react-redux';
import { getUser, errorImg } from '../redux/data';

const mapStateToProps = (state) => ({
    users: getUser(state),
})

const defaultPhoto = "https://www.kindpng.com/picc/m/451-4517876_default-profile-hd-png-download.png";

export const CustomDrawer = connect(mapStateToProps, {errorImg})(({navigation, users, errorImg}) => {
    return(
        <View style = {styles.container}>
            <View style = {styles.userInfoWrapper}>
                <View style = {styles.userInfo}>
                    <Image 
                        onError = {() => errorImg({defaultPhoto: defaultPhoto})}
                        resizeMode = "cover"
                        style = {styles.userImg}
                        source = {{uri: users.photo}}
                    />
                    <DefText numberOfLines = {1} weight = "regular" style = {styles.username}>
                        {users.username}
                    </DefText>
                </View>
            </View>
            <ScrollView 
                contentContainerStyle = {{justifyContent: "center", alignItems: "center"}}
                showsVerticalScrollIndicator = {false}
                style = {styles.content}
            >
                <TouchableOpacity onPress = {() =>navigation.navigate("Create")} style = {styles.btns}>
                    <DefText weight = "bold" style = {styles.texts}>Add new list</DefText>
                </TouchableOpacity>
                <View style = {styles.spaceX}>
                    <TouchableOpacity onPress = {() =>navigation.navigate("OneTime")} style = {styles.btns}>
                        <DefText weight = "bold" style = {styles.texts}>One time list</DefText>
                    </TouchableOpacity>
                    <TouchableOpacity onPress = {() =>navigation.navigate("Regular")} style = {styles.btns}>
                        <DefText weight = "bold" style = {styles.texts}>Regular Lists</DefText>
                    </TouchableOpacity>
                    <TouchableOpacity onPress = {() =>navigation.navigate("Settings")} style = {styles.btns}>
                        <DefText weight = "bold" style = {styles.texts}>User settings</DefText>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        </View>
    )
}
)
const styles = StyleSheet.create({
    container: {

    },
    userInfoWrapper: {
        backgroundColor: "white",
        paddingVertical: 13,
        paddingLeft: 16,
        overflow: "hidden",
        // borderWidth: 1,
    },
    userInfo: {
        flexDirection: "row",
        alignItems: "center",
        overflow: "hidden",
        // borderWidth: 1,
    },
    userImg: {
        width: 50,
        height: 50,
        borderColor: COLORS.red,
        borderRadius: 200,
        borderWidth: 3
    },
    username: {
        fontSize: 24,
        color: "grey",
        lineHeight: 29,
        marginLeft: 22,
        overflow: "hidden",
        width: "65%"
        // borderWidth: 1,
    },
    content: {
        backgroundColor: COLORS.red,
        borderTopLeftRadius: 25,
        borderTopRightRadius: 25,
        width: "100%",
        height: "100%",
        paddingTop: 11,
    },
    spaceX: {
        marginTop: 27,
    },
    btns: {
        width: 251,
        height: 34,
        backgroundColor: "white",
        borderRadius: 25,
        justifyContent: "center",
        alignItems: "center",
        marginVertical: 5
    },
    texts: {
        fontSize: 14,
        textTransform: "uppercase",
        color: COLORS.red,
    },

})