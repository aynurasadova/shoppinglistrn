import React from 'react';
import { StyleSheet, View, Image, TouchableOpacity, } from 'react-native'
import { useNavigation } from '@react-navigation/native';

import images from '../style/images';
import { DefText } from './DefText';
import COLORS from '../style/color';

export const Layout = ({title, children, rightCornerBtn, btnStyle, imgSource, onPress, goBack, goBackDisabled}) => {
  const nav = useNavigation();
    return (
      <View style={styles.container}>
        <View style = {styles.heading}>
          {
            !goBackDisabled &&
            
            <View style = {styles.backBtn}>
              <TouchableOpacity onPress = {goBack}>
                <Image source = {images.backArrow} style = {styles.backArrowImg}/>
              </TouchableOpacity>
            </View>
            
          }

          <View>
            <DefText style= {styles.headingText}>{title}</DefText>
          </View>

          {
            rightCornerBtn &&
            <View style = {styles.btn}>
              <TouchableOpacity onPress = {onPress}>
                <Image style = {[styles.btnImg, btnStyle]} source = {imgSource} />
              </TouchableOpacity>
            </View>
          }
          
        </View>
        <View onPress = {() => alert("hello")} style = {styles.items}>
            {children}
        </View>
      </View>
    );
  };
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: COLORS.red,
    },
    heading: {
      flexDirection: "row",
      width: "100%",
      height: 75,
    },
    headingText: {
        fontSize: 16,
        color: "white",
        textAlign: "center",
        position: "absolute",
        top: 26,
        left: 80,
        alignSelf: "center",
        width: 250,
        marginRight: 100,
        // borderWidth: 1,
        // paddingHorizontal: 100,
        //   marginRight: "30%",
    },
    btn: {
        position: "absolute",
        top: 15,
        right: 13,
        padding: 10,
        // borderWidth: 1,
    },
    btnImg: {
      width: 30,
      height: 22,
      padding: 10,
    },
    items: {
        borderTopStartRadius: 25,
        borderTopEndRadius: 25,
        height: "89%",
        width: "100%",
        backgroundColor: "white",
        paddingHorizontal: "5%",
        paddingTop: 10
        
        // alignItems: "center",
        // borderWidth: 5,
        // borderColor: "red",
      },
      backBtn: {
        position: "absolute",
        top: 18,
        left: 13,
        padding: 5,
        // borderWidth: 1
      },
      backArrowImg: {
        width: 32,
        height: 32,
      },

  });
  