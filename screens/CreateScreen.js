import React, { useState, useEffect, } from 'react';
import { connect } from 'react-redux';
import { 
  StyleSheet, 
  View, 
  TextInput, 
  TouchableOpacity, 
} from 'react-native'

import { Layout } from '../commons';
import COLORS from '../style/color';
import { DefText } from '../commons';
import { addList, getLists } from '../redux/data';
import { createID } from '../utils/uniqueID';

const mapStateToProps = (state) =>({
  lists: getLists(state),
});

export const CreateScreen = connect(mapStateToProps, { addList })(({ addList, navigation, lists}) => {
  
  const [type, setType] = useState("regular");
  const [fields, setFields] = useState({
    name: "",
    regular: true,
  });
  const [id, setID] = useState(null);
  

  const fieldChangeHandler = (name, value) => {
    setFields((fields) => ({
      ...fields,
      [name]: value,
    }))
  }

  const isRegular = type === "regular";
  const createHandler = () => {
    if(fields.name.trim() !==""){
      addList({id: id, name: fields.name, regular: fields.regular});
      setFields({
        name: "",
        regular: true,
      });
      navigation.navigate("SingleListDetail",{listID: id, typeIsRegular: isRegular ? true : false})
      setType("regular");
    }
  }

  useEffect(() => {
    setID(createID())
  }, [lists])

    return (
      <Layout
        goBackDisabled = {true} 
        title = "New List">
        <View style = {styles.container}>
          <DefText style = {
            [
              styles.descText, 
              styles.textAlign
            ]
            }>list name</DefText>
          <TextInput 
            value = {fields.name}
            onChangeText = {(value) => fieldChangeHandler("name", value)}
            style = {
              [
                styles.listNameInput, 
                styles.textAlign, 
                styles.commonStyles
              ]
          } 
          />

          <View style = {styles.row}>
            <TouchableOpacity 
              onPress = {() => {
                fieldChangeHandler("regular", false)
                setType("oneTime")
            }}
              style = {
                [
                  styles.radioBtnOnFocus, 
                  styles.commonStyles, 
                  {backgroundColor: isRegular ? "rgba(0,0,0,0.025)" : COLORS.lightGrey}
                ]
              }
            >
              <DefText 
                weight = {isRegular ? "medium" : "bold"}
                style = {
                  [
                    styles.textAlign, 
                    styles.radioBtnText,
                  ]
                  }
                >One Time</DefText>
            </TouchableOpacity>
            <TouchableOpacity 
              onPress = {() => {
                fieldChangeHandler("regular", true)
                setType("regular")
            }}
              style = {
                [
                  styles.radioBtnOnFocus, 
                  styles.commonStyles,
                  {backgroundColor: isRegular ? COLORS.lightGrey: "rgba(0,0,0,0.025)"}
                ]
              }
            >
              <DefText 
                weight = {isRegular ? "bold": "medium"}
                style = {
                    [
                      styles.textAlign, 
                      styles.radioBtnText
                    ]
                  }
                >Regular</DefText>
            </TouchableOpacity>   
          </View>

          <TouchableOpacity 
            onPress = {createHandler}
            style = {[styles.createBtn, styles.commonStyles]}>
            <DefText weight = "bold" style = {[styles.createBtnText, styles.textAlign]}>Create List</DefText>
          </TouchableOpacity>
        </View>
      </Layout>
    );
  })

 
const styles = StyleSheet.create({
  container: {
    marginTop: 2
  },
  textAlign: {
    textAlign: "center",
  },
  commonStyles: {
    height: 45,
    borderRadius: 50,
    marginTop: 12
  },

  descText: {
    color: "rgba(0,0,0,.7)",
    fontSize: 12,
    lineHeight: 15,
    marginBottom: -7
  },
  listNameInput: {
    backgroundColor: COLORS.lightGrey,
    fontSize: 18,
  },
  row: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  radioBtnOnFocus: {
    // backgroundColor: COLORS.lightGrey,
    justifyContent: "center",
    width: "48%",
  },
  radioBtnText: {
    fontSize: 12,
    lineHeight: 15,
  },
  createBtn: {
    justifyContent: "center",
    backgroundColor: COLORS.red,
  },
  createBtnText: {
    textTransform: "uppercase",
    color: "white",
  },
})

  