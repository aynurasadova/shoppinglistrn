import React, { useState } from 'react';
import { 
  StyleSheet, 
  View, 
  TextInput, 
  TouchableOpacity, 
  Image 
} from 'react-native'
import { Layout, DefText } from '../commons';
import COLORS from '../style/color';
import { changeUserInfo } from '../redux/data';
import { connect } from 'react-redux';

export const SettingsScreen = connect(null, { changeUserInfo })(({changeUserInfo, navigation}) => {

  const [fields, setFields] = useState({
    userName: "",
    imgUri: "",
  })

  const fieldsChangeHandler = (name, value) => {
    setFields((fields) => ({
      ...fields,
      [name]: value,
    }))
  }
  
  const saveHandler = () => {
    const url = fields.imgUri;
    const regexp = /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
    if (url != "" && fields.userName !== "") {
        if (!regexp.test(url)) {
            alert("Please enter valid url.");
        } else {

          changeUserInfo({ userName: fields.userName, imgUri: fields.imgUri })
          setFields({
            imgUri: "",
            userName: "",
          })
          navigation.navigate("HomeStack");
        }
    }
    else {
      alert("Please fill the fields correctly.");
    }
  }

  return (
    <Layout
    goBackDisabled = {true}
    title = "User Settings"
    >
      <View style = {styles.container}>
        <DefText style = {
          [
            styles.descText, 
            styles.textAlign
          ]
          }>username</DefText>
        <TextInput 
          value = {fields.userName}
          onChangeText = {(value) => fieldsChangeHandler("userName", value)}
          style = {
            [
              styles.listNameInput, 
              styles.textAlign, 
              styles.commonStyles
            ]
        } 
        />
        <DefText style = {
          [
            styles.descText, 
            styles.textAlign
          ]
          }>avatar uri</DefText>
        <TextInput 
          value = {fields.imgUri}
          onChangeText = {(value) => fieldsChangeHandler("imgUri", value)}
          style = {
            [
              styles.listNameInput, 
              styles.textAlign, 
              styles.commonStyles
            ]
        } 
        />
        <TouchableOpacity 
          onPress = {saveHandler}
          style = {[styles.createBtn, styles.commonStyles]}>
          <DefText weight = "bold" style = {[styles.createBtnText, styles.textAlign]}>Save Changes</DefText>
        </TouchableOpacity>
      </View>
    </Layout>
  );
}
)

  const styles = StyleSheet.create({
    container: {
      marginTop: 2
    },
    textAlign: {
      textAlign: "center",
    },
    commonStyles: {
      height: 45,
      borderRadius: 50,
      marginTop: 5,
      marginBottom: 8
    },
  
    descText: {
      color: "rgba(0,0,0,.7)",
      fontSize: 12,
      lineHeight: 15,
    },
    listNameInput: {
      backgroundColor: COLORS.lightGrey,
      fontSize: 18,
      paddingHorizontal: 30,
    },
    createBtn: {
      justifyContent: "center",
      backgroundColor: COLORS.red,
    },
    createBtnText: {
      textTransform: "uppercase",
      color: "white",
    },
  })
  