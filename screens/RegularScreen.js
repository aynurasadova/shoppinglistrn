import React from 'react';
import { FlatList } from 'react-native';
import { connect } from 'react-redux';
import { useNavigation } from '@react-navigation/native';

import images from '../style/images';
import { ListSection } from '../components';
import { Layout } from '../commons';
import { getLists } from '../redux/data';

const mapStateToProps = (state) => ({
  lists: getLists(state)
})

export const RegularScreen = connect(mapStateToProps)(({lists}) => {
  
  const navigation = useNavigation();
 
  const drawerHandler = () => navigation.openDrawer();

    return (
      <Layout
        rightCornerBtn = {true} 
        title = "Regular Lists"
        imgSource = {images.drawerImg} 
        onPress = {drawerHandler}
        goBackDisabled = {true}
      >
        <FlatList 
          showsVerticalScrollIndicator = {false}
          data = {lists}
          renderItem = {({item}) => (
            <ListSection 
              key = {item.id}   
              regularity = {true} 
              list = {item} />
            )
          }        
        />
      </Layout>
    );
  })
  
