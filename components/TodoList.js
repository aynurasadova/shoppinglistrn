import React from 'react';
import { 
  StyleSheet, 
  View, 
  TouchableOpacity,
  Image 
  } from 'react-native'

import { DefText } from '../commons';
import COLORS from '../style/color';
import images from '../style/images';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';

export const TodoList = ({ todo, deleteOnPress, editOnPress, isStatic, isAddingMode, toggleOnPress, isOnEditMode }) => {

  const { name, amount, unit, done } = todo;
    return (
      <>
        { isStatic &&
            <View  style = {{opacity: done ? 0.5 : 1}}>
            <TouchableOpacity onLongPress = {toggleOnPress} style = {[styles.todosRow, {paddingHorizontal: 15, paddingVertical: 10}]}>
              <View>
                <DefText numberOfLines = {1} style = {[styles.todosText, {width: 260}]}>{name}</DefText>
              </View>

              <View>
                <DefText style = {styles.todosText}>x{amount} {unit}</DefText>
              </View>
            </TouchableOpacity>
            </View>
        }            
        { isAddingMode &&
            <TouchableWithoutFeedback style = {styles.todosRow}>
              <View style = {styles.editRow}> 
                <TouchableOpacity disabled = {done ? true : false} onPress = {editOnPress} style=  {[styles.editIcon, styles.btn, {opacity: done ? 0.5 : 1}]}>
                  <Image style = {styles.editImg} source = {images.newNoteIcon}/>
                </TouchableOpacity>
                <View style = {{marginLeft: 15,}}>
                  <DefText numberOfLines = {1} style = {{width: 190}}>{name}</DefText>
                </View>
              </View>

              <View style = {styles.editRow}> 
                <View style = {{ paddingLeft: 5, width: 60}}>
                  <DefText numberOfLines = {1}>x{amount} {unit}</DefText>
                </View>
                <TouchableOpacity onPress = {deleteOnPress} style=  {[styles.deleteIcon, styles.btn]}>
                  <Image style = {styles.deleteImg} source = {images.deleteImg}/>
                </TouchableOpacity>
              </View>
              
            </TouchableWithoutFeedback>
        }
      </>
    );
  };
  
  const styles = StyleSheet.create({
    todosCounterText: {
      fontSize: 17,
    },

    todosRow: {
      flexDirection: "row",
      justifyContent: "space-between",
      borderWidth: 2,
      borderRadius: 30,
      borderColor: COLORS.yellow,
      marginVertical: 7,
      width: "100%",
      alignItems: "center",
      overflow: "hidden",
    },
    editRow: {
      flexDirection: "row",
      alignItems: "center",
    },
    editIcon: {
      backgroundColor: COLORS.yellow,
      padding: 10,
      borderRadius: 25,
      zIndex: 40
    },
    editImg: {
      width: 20,
      height: 20,
    },
    deleteIcon: {
      backgroundColor: COLORS.red,
      padding: 10,
      borderRadius: 25,
      zIndex: 40,
    },
    deleteImg: {
      width: 20,
      height: 20,
    },
  });
  