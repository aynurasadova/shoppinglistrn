import React from 'react';
import { 
  StyleSheet, 
  View, 
  TouchableOpacity, 
  Alert,
} from 'react-native'
import { useNavigation } from '@react-navigation/native';
import { connect } from 'react-redux';

import COLORS from '../style/color';
import { getLists, deleteList } from '../redux/data';
import { DefText } from "../commons";
const mapStateToProps = (state) => ({
  lists: getLists(state)
})


export const ListItemsContainer = connect(mapStateToProps, { deleteList })(({deleteList, list, notOpacity, regularForReset, listID}) => {
  const nav = useNavigation();
    
    const {name, regular, todos} = list;
    let total = todos ? todos.length : 0;
    let bought = 0;
    for(let todo in todos) {
      if(todos[todo].done) {
        bought ++;
      }
    }
    let isFinished = null;
    if(total === bought && total !== 0 && bought !== 0 && !notOpacity) {
      isFinished = true;
    }

    const deleteListHandle = () => {
      Alert.alert("Do you really want to delete list ?", "If yes, click delete", [
        {
          text: "cancel",
          style: "cancel",
        },
        {
          text: "delete",
          onPress: () => {
            deleteList({listID: listID,}) 
          }
        }
      ])
     
    }

    let progressDonePercentage = (bought / total)*100;
    let totalChecker = total === 0;
    return (
      <View style = {{opacity: (isFinished && !regular)? 0.3 : 1}}>
        <View style = {styles.singleItemWrapper}>
          <TouchableOpacity 
            onLongPress = {deleteListHandle} 
            onPress = {() => {
              nav.navigate("SingleListDetail",{listID, regularForReset})}
            }
          >
            <View style = {styles.singleItem}>
              <View style = {styles.listName}>
                <DefText numberOfLines = {1} weight = "bold" style = {styles.itemDesc}>{name}</DefText>
              </View>
              
              <View style = {styles.itemProgress}>
                <DefText weight = "bold" style = {styles.itemDesc}>{bought} / {total} </DefText>
              </View>
            </View>

            <View style = {styles.progressBar}>
              <View style= {[styles.progressDone, {width: totalChecker ? 0 :`${progressDonePercentage}%`}]} />
            </View>
          </TouchableOpacity>
        </View>
      </View>
            
    );
  });
  
  const styles = StyleSheet.create({
    
    singleItemWrapper: {
      borderWidth: 3,
      borderColor: COLORS.yellow,
      borderRadius: 8,
      paddingHorizontal: "4%",
      paddingVertical: 10, 
      marginVertical: 10,
      overflow: "hidden",
      width: "100%",
    },
    listName: {
      width: "70%",
    },
    singleItem: {
      flexDirection: "row",
      justifyContent: "space-between",

    },
    itemDesc: {
      fontSize: 17,
      lineHeight: 22,
      overflow: "hidden",
    },
    progressBar: {
      height: 19,
      backgroundColor: COLORS.lightGrey,
      marginTop: 7,
      marginBottom: 6,
      borderRadius: 20,
      overflow: "hidden",
    },
    progressDone: {
      backgroundColor: COLORS.yellow,
      height: "100%",
      borderRadius: 20,
    }
  });
  