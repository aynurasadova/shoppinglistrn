import React from 'react';

import { ListItemsContainer } from './ListItemsContainer';

export const ListSection = ({list, regularity}) => {
  const { regular, id } = list;
    return (
                <>
                  {
                    regularity ?
                  (regular &&
                  <ListItemsContainer 
                    regularForReset = {true} 
                    listID = {id} 
                    key = {id} 
                    notOpacity = {true} 
                    list = {list}
                  />
                  ):
                  (
                    !regular &&
                    <ListItemsContainer 
                      regularForReset = {false} 
                      listID = {id} 
                      key = {id} 
                      notOpacity = {false} 
                      list = {list}
                    />
                  )

                  }
                </>
    );
  };
  
  